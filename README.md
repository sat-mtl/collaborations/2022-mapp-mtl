2022 - MAPP_MTL
===============

Everything related to SAT's participation in the MAPP_MTL festival - September 2022.

3D printing slicer and gcode files can be found at [https://gitlab.com/sat-mtl/metalab/hardware/3d-print-assets/-/tree/main/MAPP](https://gitlab.com/sat-mtl/metalab/hardware/3d-print-assets/-/tree/main/MAPP).