Introduction au videomapping
============================

Cette présentation est basée sur Splash, et utilise le fichier de configuration dédié situé dans `../splash`.

Pour la lire, vous aurez besoin de `mdp`:

```bash
sudo apt install mdp
mdp intro_mapping.md
```
