-> # Introduction au videomapping <-

-> (avec [Splash](https://splashmapper.xyz)) <-

-> [SAT]Metalab x Lab148 <-

----------------

-> [SAT]Metalab, laboratoire de recherche de la SAT <-
=================================================

* Laboratoire de recherche de la **SAT**
    * équipe de chercheurs/développeurs
* Mission : créer des outils pour faciliter l'accès aux nouvelles technologies
* Thèmes de recherche : téléprésence, immersion, mapping vidéo, son spatialisé, ...
* Valorisation :
    * transfert technologique
    * communications scientifiques
    * logiciels libres (GPL, LGPL)
* [https://sat.qc.ca/fr/metalab](https://sat.qc.ca/fr/metalab)

---

-> Coopérative Lab148 <-
========================

* Accompagnement en R&D pour les installations médiatiques
* Conceptualisation, prototypage, transfert de connaissance
* Développement logiciels, conception de systèmes numériques
* [https://lab148.xyz](https://lab148.xyz)

---

-> ## Les besoins du vidéo-mapping <-

* Surfaces de projection
* Vidéo-projecteur(s)
* Modèle 3D des surfaces de projection
* Contenu à présenter
* Connaître la position et l'orientation des vidéo-projecteurs

---

-> ## Obtention d'un modèle 3D <-

* Par impression 3D à partir d'un modèle
* Par prise de mesures et création manuelle d'un modèle
* Par photogrammétrie

---

-> ## Installation des vidéo-projecteurs <-

* Couvrir les surfaces de projection
* Limiter les distortions
* Maximiser la zone de projection au focus
* Uniformiser les paramètres des vidéo-projecteurs
* Désactiver les corrections internes des vidéo-projecteurs

---

-> ## Calibrage des projecteurs et arrimage du modèle 3D à la surface de projection <-

* Points de calibratage
* Déterminer la position, l'orientation, la focale et certaines déformations des vidéo-projecteurs

---

-> ## Blending/mélange des zones de projection <-

Problème avec de multiples projections qui se superposent :

* Luminosité variable sur les surfaces de projection
* Qualité inconstante de la projection

---

-> ## Contenu <-

* Image
* Vidéo
* Flux en direct (NDI)

---

-> ## Et après? <-

* Interactivité
* Projection sur des objects mobiles
* Projection sur des gens

---

-> ## Ressources <-

- https://fr.wikipedia.org/wiki/Mapping_vidéo
- https://projection-mapping.org/
- https://en.wikipedia.org/wiki/Image_warping
- http://paulbourke.net/dome/fisheyewarp/
